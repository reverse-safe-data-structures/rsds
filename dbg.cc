	/*
		RSDS: Reverse-safe Text Indexing
		Copyright (C) 2020 Grigorios Loukides, Solon P. Pissis, Huiping Chen
		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program.  If not, see <http://www.gnu.org/licenses/>.
	**/
	#include <boost/math/special_functions/factorials.hpp>
	#include <boost/math/special_functions.hpp>
	using namespace boost::math;

	#define DEBUG false
	#define PRUNE_DET true   
	#define GAB_OPT_IN_DB false    
	#define GAB_OPT_IN_NP true  

	#include <stdio.h>
	#include <math.h>
	#include <stdlib.h>
	#include <string.h>
	#include <getopt.h>
	#include <assert.h>
	#include <sys/time.h>
	#include <iostream>
	#include <map>
	#include <set>
	#include <unordered_set>
	#include <unordered_map>
	#include <vector>
	#include <stack>
	#include <fstream>

	#include "rsds.h"
	#include "dbg.h"

	#ifdef _USE_64
	#include <divsufsort64.h>                                      
	#endif

	#include <sdsl/bit_vectors.hpp>				

	using namespace sdsl;
	using namespace std;

	#include <Eigen/Sparse>
	typedef Eigen::Triplet<double> T;
	typedef Eigen::SparseMatrix<double> SparseMatrixType;


	void free_arrays(INT *SA, INT *LCP, INT *invSA)
	{
	  free(invSA);
	  free(SA);
	  free(LCP);
	}

	int binarydB_doubling_prefix( unsigned char* seq, unsigned int z, unsigned int k)
	{
			INT *SA;
			INT *LCP;
			INT *invSA;
			INT n=strlen ( ( char * ) seq );
		
			string tmpx((const char*)seq);

			INT r_S=-1;

			cout<<"Initial suffix array construction: "<<endl;
			
			
			SA = ( INT * ) malloc( ( n ) * sizeof( INT ) );
			invSA = ( INT * ) calloc( n , sizeof( INT ) );
			LCP = ( INT * ) calloc  ( n, sizeof( INT ) );			
			
			if( ( SA == NULL) )
			{
					return ( 0 );
			}

			if( ( invSA == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for invSA.\n" );
					return ( 0 );
			}
			
			if( ( LCP == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for LCP.\n" );
					return ( 0 );
			}
			
			suffix_array_construction_nomalloc(seq, SA, LCP, invSA, r_S);

//			suffix_array_construction(seq, SA, LCP, invSA, r_S);
 		    
			
			cout<<"r(S): "<<r_S-1<<endl;

			double log_z=log(z);

			INT l=0;
			INT r;

			if(GAB_OPT_IN_DB) 
					r=r_S;  
			else
					r=n;


			srand(time(NULL)); 

		   if(l<0)
			   l=0;



		  double time_for_doubling=0.0; 

		int cnt=0; 
		//int no_iter=0;

		int prefix_sz=2*k<n?2*k:n;
		 
		int largest_prefix_iter= ceil(log((double)n/(double)k)/log(2));
	   
		 
		bool largest_prefix_found=false;
		bool old_prefix=false;
		unsigned char *prefix=NULL;
		INT r_S_pref=-1;

		bool unique_prefix=false;  
		int no_of_unique=0, no_of_non_unique=0;

			 
		INT d=floor((double)(l+r)/2.0);

		while(l<r)
		{
			if(!old_prefix)   
			{
		
				prefix=(unsigned char *)strndup((const char *)seq, prefix_sz);
				
				r_S_pref=-1;

				free_arrays(SA, LCP, invSA);
				
				SA = ( INT * ) malloc( ( n ) * sizeof( INT ) );
				invSA = ( INT * ) calloc( n , sizeof( INT ) );
				LCP = ( INT * ) calloc  ( n, sizeof( INT ) );			

				suffix_array_construction_nomalloc(prefix, SA, LCP, invSA, r_S_pref);
				//suffix_array_construction(prefix, SA, LCP, invSA, r_S_pref);

				if(r_S_pref<d)
				{
					unique_prefix=true;
					no_of_unique++;
				}
				else
				{
					unique_prefix=false;
					no_of_non_unique++;
				}
				old_prefix=false;
			}
		 
		  
			d=floor((double)(l+r)/2.0);

			if(!largest_prefix_found)
			{
				while(cnt < largest_prefix_iter)
				{
					if(!unique_prefix && dBgraph (prefix, d, SA, LCP, invSA, log_z,prefix_sz ))
				       {
						l=d+1;
		
						old_prefix=true;
					
							break;
					}
					else 
					{

						if(cnt==largest_prefix_iter)
						{
						   
							old_prefix=true;
							r=d;
							break;
						}
						else  
						{
							prefix_sz=prefix_sz*2<n?prefix_sz*2:n;
							 cnt++;

							if(cnt==largest_prefix_iter)
								largest_prefix_found=true;

							prefix=(unsigned char *)strndup((const char *)seq, prefix_sz);

							free ( invSA );
							free ( SA );
							free ( LCP );

							r_S_pref=-1;
							clock_t begin_sac=clock();

							SA = ( INT * ) malloc( ( n ) * sizeof( INT ) );
							invSA = ( INT * ) calloc( n , sizeof( INT ) );
							LCP = ( INT * ) calloc  ( n, sizeof( INT ) );			

							suffix_array_construction_nomalloc(prefix, SA, LCP, invSA, r_S_pref);
							//suffix_array_construction(prefix, SA, LCP, invSA, r_S_pref);
							clock_t end_sac=clock();
					
							time_for_doubling+=double(end_sac-begin_sac)/CLOCKS_PER_SEC;
								  
							if(r_S_pref < d)
							{
								unique_prefix=true;
								no_of_unique++;
							}
							else
							{
								unique_prefix=false;
								no_of_non_unique++;

							}

							if(prefix_sz==n) 
							{
								//old_prefix==true;
								break;  //new addition
							}
						}
					}
			   }

			}
			else
			{				

				if(r_S_pref>=d && dBgraph (prefix, d, SA, LCP, invSA, log_z,prefix_sz ))
				{
					l=d+1;     
				
				}
				else
				   r=d;
			}
		}

			if(l>0)
			{
				cout<<"Prefix size: "<<prefix_sz<<" Answer d = "<<l-1<<endl;
				d=l-1;
			}

			if(l==0)
				cout<<"FAIL\n";

		cout<<"Unique_prefixes: "<<no_of_unique<<" non_unique_prefixes: "<<no_of_non_unique<<" Ratio unique/all: "<<(double)no_of_unique/((double)(no_of_unique+no_of_non_unique))<<endl;
		free(prefix);
		 free ( invSA );
		 free ( SA );
		 free ( LCP );


		return d;

	}

   
	int binarydB_no_prefix( unsigned char* seq, unsigned int z, unsigned int k)
	{
		INT *SA; 
			INT *LCP;
			INT *invSA;
			INT n=strlen ( ( char * ) seq );
			string tmpx((const char*)seq);	
			INT r_S=-1;
			
			SA = ( INT * ) malloc( ( n ) * sizeof( INT ) );
			invSA = ( INT * ) calloc( n , sizeof( INT ) );
			LCP = ( INT * ) calloc  ( n, sizeof( INT ) );			
			
			if( ( SA == NULL) )
			{
					return ( 0 );
			}

			if( ( invSA == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for invSA.\n" );
					return ( 0 );
			}
			
			if( ( LCP == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for LCP.\n" );
					return ( 0 );
			}
			
			
			suffix_array_construction_nomalloc(seq, SA, LCP, invSA, r_S);
			double log_z=log(z);

			INT l=0;
			INT r=n;					

			srand(time(NULL));        
			
			INT d=0;

			while(l<r)
			{     
				d=floor((double)(l+r)/2.0);
					if(dBgraph (seq, d, SA, LCP, invSA, log_z,n ))
					{			
						l=d+1;      		                                
					}
					else 
					{
						r=d;		                                
					}                
			}
			if(l>0){
				cout<<"Answer d = "<<l-1<<endl;
			d=l-1;               
			}
			if(l==0)  cout<<"FAIL\n";
		 free ( invSA );
		 free ( SA );
		 free ( LCP );
		 return d;
	}
	    
	int binarydB_no_prefix_gab( unsigned char* seq, unsigned int z, unsigned int k)
	{
			INT *SA;
			INT *LCP;
			INT *invSA;
			INT n=strlen ( ( char * ) seq );
			string tmpx((const char*)seq);
			INT r_S=-1;
			
			
			SA = ( INT * ) malloc( ( n ) * sizeof( INT ) );
			invSA = ( INT * ) calloc( n , sizeof( INT ) );
			LCP = ( INT * ) calloc  ( n, sizeof( INT ) );			
			
			if( ( SA == NULL) )
			{
					return ( 0 );
			}

			if( ( invSA == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for invSA.\n" );
					return ( 0 );
			}
			
			if( ( LCP == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for LCP.\n" );
					return ( 0 );
			}
			
			suffix_array_construction_nomalloc(seq, SA, LCP, invSA, r_S);
			
			
			double log_z=log(z);

			INT l=0;
			INT r=0;
			if(GAB_OPT_IN_NP)    
			{		
				r=r_S;  
			}
			else
				r=n;


			srand(time(NULL)); 			
			INT d=0;

			while(l<r)
			{
					d=floor((double)(l+r)/2.0);
					if(dBgraph (seq, d, SA, LCP, invSA, log_z,n ))
					{
						l=d+1;
					}
					 else 
					{                  
						r=d;
					}
			}
			if(l>0){
				cout<<"Answer d = "<<l-1<<endl;
				d=l-1;
			}
			if(l==0)  cout<<"FAIL\n";
		 free ( invSA );
		 free ( SA );
		 free ( LCP );
		 return d;
	}
    
	int binarydB_no_prefix_exp( unsigned char* seq, unsigned int z, unsigned int k)
	{
			INT *SA;
			INT *LCP;
			INT *invSA;
			INT n=strlen ( ( char * ) seq );
			string tmpx((const char*)seq);
			INT r_S=-1;
			
			SA = ( INT * ) malloc( ( n ) * sizeof( INT ) );
			invSA = ( INT * ) calloc( n , sizeof( INT ) );
			LCP = ( INT * ) calloc  ( n, sizeof( INT ) );			
			
			if( ( SA == NULL) )
			{
					return ( 0 );
			}

			if( ( invSA == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for invSA.\n" );
					return ( 0 );
			}
			
			if( ( LCP == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for LCP.\n" );
					return ( 0 );
			}
			
			suffix_array_construction_nomalloc(seq, SA, LCP, invSA, r_S);
			
			double log_z=log(z);

			INT l=0;
			INT r;
			r=n;
			srand(time(NULL));   
			INT i=2;
  		        while(i<n && dBgraph (seq, i, SA, LCP, invSA, log_z,n )){
				i=i*2;
			}
			l=i/2;
			r=(i<r)?i:r;
			//int no_iter=0;
			INT d;

			   while(l<r)
			  {

					d=floor((double)(l+r)/2.0);

					if(dBgraph (seq, d, SA, LCP, invSA, log_z,n ))		
					{
						l=d+1;
					}
					else  //a_d < z
					{                  
							r=d;
					}
			   }
			if(l>0)
			{
				cout<<"Answer d = "<<l-1<<endl;
				d=l-1;
			}
			if(l==0) cout<<"FAIL\n";
		 
		 free ( invSA );
		 free ( SA );
		 free ( LCP );
		 return d;
	}
	void binarydB( unsigned char* seq, unsigned int z)
	{
		INT *SA;
		INT *LCP;
		INT *invSA;

			INT n=strlen ( ( char * ) seq );
	 
			INT r_S=-1;

			suffix_array_construction(seq, SA, LCP, invSA, r_S);

			double log_z=log(z);

			INT l=0;
			INT r;

			if(GAB_OPT_IN_DB)      
			r=r_S;   
		else
			r=n;


		   srand(time(NULL));   


		   if(l<0)
		   l=0;



			while(l<r)
		{
			INT d=floor((double)(l+r)/2.0);

			if(d==0)
			{
				l=0;
				break;		
			}
			if(dBgraph (seq, d, SA, LCP, invSA, log_z,n ))
				l=d+1;
			else
				r=d;					
		}
		if(l>0)
		{
			cout<<"Answer d = "<<l-1<<endl;
		}	
		if(l==0)
			cout<<"FAIL\n";

			

		free ( invSA );
		free ( SA );
		free ( LCP );

	}

	double factorial_compute(unsigned int x)
	{
			try{
					return factorial<double>(x);
			}
			catch(exception& e)
			{
					cout<< e.what() <<endl;
			}
			
			return 0;
	}


	unsigned int LCParray ( unsigned char * x, INT n, INT * SA, INT * ISA, INT * LCP )
	{										
		INT i=0, j=0;

		LCP[0] = 0;
		for ( i = 0; i < n; i++ )
			if ( ISA[i] != 0 ) 
			{
				if ( i == 0) j = 0;
				else j = (LCP[ISA[i-1]] >= 2) ? LCP[ISA[i-1]]-1 : 0;
				while ( x[i+j] == x[SA[ISA[i]-1]+j] )
					j++;
				LCP[ISA[i]] = j;
			}

		return ( 1 );
	}

	int suffix_array_construction(unsigned char *x, INT *&SA, INT *&LCP, INT *&invSA, INT &r_S)
	{

	   INT n = strlen ( ( char * ) x );

			SA = ( INT * ) malloc( ( n ) * sizeof( INT ) );
			if( ( SA == NULL) )
			{
					return ( 0 );
			}

			#ifdef _USE_64
			if( divsufsort64( x, SA,  n ) != 0 )
			{
					exit( EXIT_FAILURE );
			}
			#endif

			invSA = ( INT * ) calloc( n , sizeof( INT ) );
			if( ( invSA == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for invSA.\n" );
					return ( 0 );
			}

			for ( INT i = 0; i < n; i ++ )
					invSA [SA[i]] = i;


			LCP = ( INT * ) calloc  ( n, sizeof( INT ) );
			if( ( LCP == NULL) )
			{
					fprintf(stderr, " Error: Cannot allocate memory for LCP.\n" );
					return ( 0 );
			}

			if( LCParray( x, n, SA, invSA, LCP ) != 1 )
			{
					fprintf(stderr, " Error: LCP computation failed.\n" );
					exit( EXIT_FAILURE );
			}

		
	   if(GAB_OPT_IN_DB || GAB_OPT_IN_NP)
	   {       INT maxLCP=-1;

		   for(int i=0;i<n;++i)
		   {
		  if(LCP[i]>maxLCP)
			maxLCP=LCP[i];
		   }

		   r_S=maxLCP+1;   
	   }
	  return 0;
	}
int suffix_array_construction_nomalloc(unsigned char *x, INT *SA, INT *LCP, INT *invSA, INT &r_S)
	{

	   INT n = strlen ( ( char * ) x );

			
			
			#ifdef _USE_64
			if( divsufsort64( x, SA,  n ) != 0 )
			{
					exit( EXIT_FAILURE );
			}
			#endif

			
			

			for ( INT i = 0; i < n; i ++ )
					invSA [SA[i]] = i;

			
			

			if( LCParray( x, n, SA, invSA, LCP ) != 1 )
			{
					fprintf(stderr, " Error: LCP computation failed.\n" );
					exit( EXIT_FAILURE );
			}

		
	   if(GAB_OPT_IN_DB || GAB_OPT_IN_NP)
	   {       INT maxLCP=-1;

		   for(int i=0;i<n;++i)
		   {
		  if(LCP[i]>maxLCP)
			maxLCP=LCP[i];
		   }

		   r_S=maxLCP+1;   
	   }
        return 0;
	}
	
	bool dBgraph (  unsigned char * x, unsigned int d, INT *SA, INT *LCP, INT *invSA, double log_z, INT n)
	{

			if(d==0)
			return true;

		INT cluster_id=(INT) 0;
		INT *C=new INT[n];

		for (INT i=0; i<n ; i++)
		{
			if(LCP[i]>=d-1)
			{
				C[i]=(INT)cluster_id;			
				
			}
			else
			{
				cluster_id++;
				C[i]=cluster_id;
				
			}	
		
		}


		multimap<int, int> map_for_outdegree;


		multimap<int, int> map_for_indegree;


		unordered_map<int, int> node_id_consec; 
		int node_id_consec_ind=0;

			unordered_set<INT> distinct_nodes;
	 
		for(INT i=0; i<=n-d ; ++i)
		{
			INT cluster_id_pref=C[invSA[i]]; 
			INT cluster_id_suff=C[invSA[i+1]]; 

			distinct_nodes.insert(cluster_id_pref);
					distinct_nodes.insert(cluster_id_suff);


			map_for_outdegree.insert(std::pair<int,int>(cluster_id_pref,cluster_id_suff));
			map_for_indegree.insert(std::pair<int,int>(cluster_id_suff,cluster_id_pref));
			
					unordered_map<int, int>::const_iterator nic_find=node_id_consec.find(cluster_id_pref);

			if(nic_find==node_id_consec.end())
			{	node_id_consec[cluster_id_pref]=node_id_consec_ind;
						node_id_consec_ind++; 
				}

			unordered_map<int, int>::const_iterator nic_find2=node_id_consec.find(cluster_id_suff);

					if(nic_find2==node_id_consec.end())
					{       node_id_consec[cluster_id_suff]=node_id_consec_ind;
							node_id_consec_ind++;
					}

		}



		 INT matrix_dim=distinct_nodes.size();

		 std::vector<T> tripletList;
	  
		int node_t=C[invSA[n-d+1]];
	 
		std::pair <std::multimap<int,int>::iterator, std::multimap<int,int>::iterator> ret_outdegree;
		std::unordered_map<int,int> node_outdegree;
	   
		unordered_set<int> used_keys; 

		unordered_map<int,int> a_uu;

	   
		
		double denominator_log_sum=0.0; 
		double nominator_log_sum=0.0;

	  
		for(multimap<int,int>::const_iterator it=map_for_outdegree.begin();it!=map_for_outdegree.end();++it)
		{
			ret_outdegree = map_for_outdegree.equal_range(it->first);
		   
			   
			std::unordered_map<int,int>::const_iterator it3=node_outdegree.find(it->first);
				if(it3==node_outdegree.end())
			{
			node_outdegree.insert(std::pair<int,int>(it->first,1));
			}    	    
			else
			node_outdegree[it->first]++;

			  
			   unordered_set<int>::const_iterator used_keys_it=used_keys.find(it->first);
			   
			   if(used_keys_it==used_keys.end())
			   {
		  

					used_keys.insert(it->first);


				  unordered_map<int,int> edge_multiplicity;

			 for (std::multimap<int,int>::iterator it2=ret_outdegree.first; it2!=ret_outdegree.second; ++it2)
			 { 		
				unordered_map<int,int>::const_iterator it3=edge_multiplicity.find(it2->second);
				if(it3==edge_multiplicity.end())
					edge_multiplicity.insert(std::pair<int,int>(it2->second,1));
				else
					edge_multiplicity[it2->second]++;
			 }
			 

				 
			 for(unordered_map<int,int>::const_iterator it4=edge_multiplicity.begin();it4!=edge_multiplicity.end();++it4)
			{
					
				  for(int x=1;x<=it4->second;++x)
					{    
					  denominator_log_sum+=log(x);
					}
	  
				
					if(it->first != it4->first)
					{
						tripletList.push_back(T(node_id_consec[it->first],node_id_consec[it4->first],-1*it4->second));
				   }
			   else  
			   {
				a_uu[it->first]=it4->second;
			   }
				   
								 
			}
			 }
			
		}

		for(unordered_map<int,int>::const_iterator itx=node_id_consec.begin(); itx!=node_id_consec.end();++itx)
		{
		

			if(itx->first == node_t)
			{
				tripletList.push_back(T(itx->second,itx->second,node_outdegree[node_t]+1-a_uu[node_t]));
			}
			else
			{
				tripletList.push_back(T(itx->second,itx->second,node_outdegree[itx->first]-a_uu[itx->first]));
			}
		}

	   
		for(unordered_map<int,int>::const_iterator it=node_outdegree.begin();it!=node_outdegree.end();++it)
		{
			 
			  if(it->first!=node_t)
			 {
				 for(int x=1;x<=it->second-1;++x)
					nominator_log_sum+=log(x);
			 }
			  else
			  {
				   for(int x=1;x<=it->second;++x)
					 nominator_log_sum+=log(x);
			  }
		}

		std::pair <std::multimap<int,int>::iterator, std::multimap<int,int>::iterator> ret_indegree;
		std::unordered_map<int,int> node_indegree;

		for(multimap<int,int>::const_iterator it=map_for_indegree.begin();it!=map_for_indegree.end();++it)
		{

			ret_indegree = map_for_indegree.equal_range(it->first);
			  
			std::unordered_map<int,int>::const_iterator it3=node_indegree.find(it->first);
			if(it3==node_indegree.end())
			 {
				node_indegree.insert(std::pair<int,int>(it->first,1));
			 }
			else
				node_indegree[it->first]++;

		}


		

	 
		SparseMatrixType m((INT)matrix_dim,(INT)matrix_dim);
		m.setFromTriplets(tripletList.begin(), tripletList.end());

	delete []C; 

	if(PRUNE_DET)
	{
	   if((nominator_log_sum - denominator_log_sum) >= log_z)
		{
			return true;
		}
	}

		Eigen::SparseLU<Eigen::SparseMatrix<double>  > solver;
		solver.analyzePattern(m);
		solver.factorize(m);

		double logdet=solver.logAbsDeterminant();

	
	   
		double log_N_gt=logdet+nominator_log_sum-denominator_log_sum;

			
		if(log_N_gt >= log_z)
		{

			return true;
		}

		return false;
	}

	void euler_path(string &x, int d, string output, INT &line_cnt)
	{
		 INT n=x.length();
		
		vector<string> B_vec;
		unordered_map<string,int> B_map;


		INT id=0;
	 
		for (int m=0;m<=n-d;m++)
		{   
			string pref=x.substr(m,d-1);
		string suff=x.substr(m+1,d-1);
		
		 
			B_vec.push_back(pref);
			B_vec.push_back(suff);
		  

			unordered_map<string,int>::const_iterator it=B_map.find(pref);
		if(it==B_map.end())
		{
			B_map.insert(std::pair<string,int>(pref,id));
			++id;
		}

		it=B_map.find(suff);
			if(it==B_map.end())
			{
				B_map.insert(std::pair<string,int>(suff,id));
			++id;
		}

		}


	 
		vector< vector<int> > adj;
		
		 adj.resize(id);

	   unordered_map<int, string> reverse_B_map;

	   for(vector<string>::const_iterator it=B_vec.begin();it!=B_vec.end();++it)
	   {
			auto it2=it;
		++it2;
		if(it2!=B_vec.end())
		{
			adj[B_map[*it]].push_back(B_map[*it2]);
		   
			reverse_B_map.insert(std::pair<int,string>(B_map[*it],*it));
			reverse_B_map.insert(std::pair<int,string>(B_map[*it2],*it2));
			++it;
		}
	   }
	   cout<<endl;
	 

	   printCircuit(adj, reverse_B_map, output, line_cnt);
	}


	void printCircuit(vector< vector<int> > adj, unordered_map<int,string> reverse_B_map, string output, INT &line_cnt) 
	{ 
		
		unordered_map<int,int> edge_count; 

		for (int i=0; i<adj.size(); i++) 
		{ 
			
			edge_count[i] = adj[i].size(); 
		} 

		if (!adj.size()) 
			return;
		stack<int> curr_path; 

		vector<int> circuit; 

		curr_path.push(0); 
		int curr_v = 0; 

		while (!curr_path.empty()) 
		{ 
			
			if (edge_count[curr_v]) 
			{ 
				curr_path.push(curr_v); 
				int next_v = adj[curr_v].back(); 

				edge_count[curr_v]--; 
				adj[curr_v].pop_back(); 
				curr_v = next_v; 
			} 


			else
			{ 
				circuit.push_back(curr_v); 
				curr_v = curr_path.top(); 
				curr_path.pop(); 
			} 
		} 


		ofstream output_file;
		output_file.open(output,std::ios_base::app);

		output_file<<">"<<line_cnt<<endl;

		 for (int i=circuit.size()-1; i>=0; i--)
			{
					if(i!=circuit.size()-1)
			{
				
				output_file<<reverse_B_map[circuit[i]].substr(reverse_B_map[circuit[i]].length()-1,1);
			}
			else
			{
				output_file<<reverse_B_map[circuit[i]];
			}

			}

		output_file<<endl;
		output_file.close();
	} 


