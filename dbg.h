/**
    RSDS: Reverse-safe Text Indexing
    Copyright (C) 2020 Grigorios Loukides, Solon P. Pissis, Huiping Chen
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifdef _USE_64
typedef int64_t INT;
#endif


#include <vector>
#include <unordered_map>
using namespace std;

unsigned int LCParray ( unsigned char * x, INT n, INT * SA, INT * ISA, INT *LCP );
void binarydB ( unsigned char* x, unsigned int z);
int suffix_array_construction(unsigned char* x, INT *&SA, INT *&LCP, INT *&invSA, INT &r_S);
int suffix_array_construction_nomalloc(unsigned char* x, INT *SA, INT *LCP, INT *invSA, INT &r_S);
int suffix_array_construction_fixed_length(unsigned char *x, INT *&SA, INT *&LCP, INT *&invSA, INT &r_S, INT &n);
int binarydB_doubling_prefix(unsigned char *x, unsigned int z, unsigned int k);
int binarydB_doubling_prefix_no_gab(unsigned char *x, unsigned int z, unsigned int k);
int binarydB_no_prefix(unsigned char *x, unsigned int z, unsigned int k);
int binarydB_no_prefix_gab(unsigned char *x, unsigned int z, unsigned int k);
int binarydB_no_prefix_exp(unsigned char *x, unsigned int z, unsigned int k);

void euler_path(std::string &x, int d, string output, INT &line_cnt);
void printCircuit(std::vector<std::vector<int> > adj,  unordered_map<int,string> reverse_B_map, string outputfile, INT &line_cnt);
