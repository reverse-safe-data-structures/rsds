RSDS: Reverse-safe Text Indexing
Copyright (C) 2020 Grigorios Loukides, Solon P. Pissis, Huiping Chen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 
COMPILATION AND EXAMPLE
------------------------
The implementation of our approach that is used in experiments 
compiles and runs on a small example with the following two commands:
sh install.sh  
sh compile.sh

The first command installs the following libraries that are needed 

Eigen: https://gitlab.com/libeigen/eigen/-/releases 
Boost: https://www.boost.org/users/download/
sdsl-lite: https://github.com/simongog/sdsl-lite

The second command compiles our program and runs a small example.

Our approach was compiled with g++ -std=c++11 -O3 --msse3 

We used g++ version (7.3.0) and it was tested on Scientific Linux 6.6.

INFORMATION ABOUT THE INPUT AND OUTPUT
---------------------------------------
Our approach 

  Input parameters (we refer to the parameters using the example in ./compile.txt):

    dataset.txt: This is the input string. It should be a single line of characters.

    z: This is the parameter z (privacy threshold).

    zrcbp or zrc or zrcb or zrce: These execute the z-RCBP, z-RC, z-RCB, and z-RCE algorithm, respectively. 

    k: This is the parameter k in the paper (the length-k substring of S).
    
    output_file: This is the output file for the alternative string that is output by the algorithm. 

For example, with input string 'abaabbabba' in test.txt, we can run z-RC with z=5 and k=3 and set the output file to be zrc_output with
./rsds test.txt 5 zrc 3 zrc_output

and one possible output is:
 
ZRC algorithm
z is 5
Answer d = 3
Returned: 3

Time needed 0.000000 secs

An alternative string in the output file zrc_output is :
abbabbaaba


Comments and Questions
----------------------
Huiping Chen
huiping.chen@kcl.ac.uk

Grigorios Loukides
grigorios.loukides@kcl.ac.uk